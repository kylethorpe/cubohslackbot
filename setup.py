from setuptools import setup

setup(name='cubohslackbot',
      version='0.1',
      description='A bot for slack',
      url='https://gitlab.com/kylethorpe/cubohslackbot',
      author='kylethorpe',
      author_email='kyle@cuboh.com',
      license='',
      packages=['cubohslackbot'],
      zip_safe=False)