import requests
import json

class SlackBot():
    def __init__(self, webhook):
        self.webhook = webhook

    def send_exception(self, origin, exception):
        headers = {"Content-type": "application/json"}
        data = {
            "blocks": [{
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"*Exception from {origin}*"
                }
            }, {
                "type": "divider"
            }, {
                "type": "context",
                "elements": [{
                    "type": "mrkdwn",
                    "text": f"{exception}"
                }]
            }]
        }
        message_res = requests.post(url=self.webhook,headers=headers,data=json.dumps(data))

